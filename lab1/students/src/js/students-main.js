window.addEventListener('load', async () =>
{
  if('serviceWorker' in navigator)
  {
    try 
    {
      //register a service worker
      const reg = await navigator.serviceWorker.register('/students/sw.js');
      console.log("SW success", reg);
    } 
    catch (error) 
    {
      console.log("SW fail");
    }
}
});


let table = document.querySelector("#student-table");
const confirmationPopup = document.querySelector("#confirmation-popup");
const confirmButton = document.querySelector("#confirm-button");
const cancelButton = document.querySelector("#cancel-button");
var rIndex, rowElements;
function deleteRow(button) {
  confirmationPopup.style.display = "block";
  confirmButton.addEventListener("click", () => {
    confirmationPopup.style.display = "none";
    var row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
  });
}
cancelButton.addEventListener("click", () => {
  confirmationPopup.style.display = "none";
});
let modifyBtn = document.querySelector("#add-edit-stud-btn");
modifyBtn.addEventListener("click", (e) => {
  e.preventDefault();
  let genders = document.querySelector("#gender-list");
  let groups = document.querySelector("#group-list");
  let groupName = groups.options[groups.selectedIndex].text;
  let fullName = document.querySelector("#full-name").value;
  let genderName = genders.options[genders.selectedIndex].text;
  let birthDay = document.querySelector("#birth-date").value;
  $('#hidden-input').val(+$('#hidden-input').val() + 1);
  let id = $('#hidden-input').val();
  const formData = { id, groupName, fullName, genderName, birthDay };

  const jsonData = JSON.stringify(formData);
  console.log('User entered:\n'+jsonData);
  console.log('Validation in server-side...');
 
  var xhr = new XMLHttpRequest();
   // send data to server
  xhr.open("POST", "validation.php", true);
  xhr.setRequestHeader("Content-Type", "application/json");
  
  var hasError = 0;
  console.log(hasError);

  xhr.send(jsonData);
  // receive error message
  xhr.onreadystatechange = async function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log("Response from server:\n"+this.responseText);
      let start = this.responseText.indexOf('{');
      let end;
      let errorMsgs = [];

      while (start !== -1) {
        end = this.responseText.indexOf('}', start) + 1;
        errorMsgs.push(JSON.parse(this.responseText.substring(start, end)));
        start = this.responseText.indexOf('{', end);
      }
      console.log(errorMsgs);
      for(let i =0; i < errorMsgs.length; i++)
      {
        if(errorMsgs[i].status === 'false') {
          Swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: errorMsgs[i].errorMessage + '!\nCorrect your data and try again'
          });
          hasError++;
        }
      }
      
      if(hasError === 0)
      {
        if (modifyBtn.innerHTML === "Add") {
          // if(hasError > 0)
          // {
          //   e.preventDefault();
          //   return false;
          // }
            table.insertAdjacentHTML(
              "beforeend",
              `<tr>
            <td><input type="checkbox" id ="myCheckbox"></td>
            <td>${groupName}</td>
            <td>${fullName}</td>
            <td>${genderName}</td>
            <td>${birthDay}</td>
            <td><span class="dot" id = "status"></span></td>
            <td>
              <i class = "material-icons edit-btn" >edit</i>
              <i id ="delete-btn" class="material-icons" onclick="deleteRow(this)">delete</i>
          </td> 
          </tr>`
            );
           
          } else {
            // if(hasError > 0)
            // {
            //   return false;
            // }
            table.rows[rIndex].cells[1].innerHTML = groupName;
            table.rows[rIndex].cells[2].innerHTML = fullName;
            table.rows[rIndex].cells[3].innerHTML = genderName;
            table.rows[rIndex].cells[4].innerHTML = birthDay;
          }
      }
    }
  }  
  

  
    document.querySelector(".add-edit-form").style.display = "none";
    makeDefaultFields();
    return true;
});
function makeDefaultFields()
{
  document.querySelector("#full-name").value = "";
  document.querySelector("#birth-date").value = "";
  document.querySelector("#group-list").value = "3";
  document.querySelector("#gender-list").value = "1";
}
function toggleDropdown(className) {
  let dropdown = document.querySelector(className);
  if (dropdown.style.display === "block") {
    dropdown.style.display = "none";
  } else {
    dropdown.style.display = "block";
  }
}

let openAddWindowBtn = document.getElementById("open-add-dialog-btn");
openAddWindowBtn.addEventListener("click", () => {
  $("#form-label").text("Add new student");
  $("#add-edit-stud-btn").text("Add");
  makeDefaultFields();
  document.querySelector(".add-edit-form").classList.add("active");
  document.querySelector(".add-edit-form").style.display = "grid";
});

document.querySelector(".close-btn").addEventListener("click", (e) => {
  e.preventDefault();
  document.querySelector(".add-edit-form").classList.remove("active");
  document.querySelector(".add-edit-form").style.display = "none";
});

function EditStudentOpen(event) {
  document.querySelector(".add-edit-form").classList.add("active");
  document.querySelector(".add-edit-form").style.display = "grid";
  $("#form-label").text("Edit student");

  $("#add-edit-stud-btn").text("Save changes");
  let activeBtn = event.target;
  rIndex = activeBtn.parentElement.parentElement.rowIndex;
  rowElements = activeBtn.parentElement.parentElement.children;
  let $groupOption = $('#group-list option')
  for (const option of $groupOption) {
      if(option.innerHTML === rowElements[1].innerHTML) {
          option.selected = true;
          break;
      }
  }
  $("#full-name").val(rowElements[2].innerHTML);
  let $genderOptions = $("#gender-list option");
  let genderValue = rowElements[3].innerHTML === "M" ? "1" : "2";
  for (var i = 0; i < $genderOptions.length; i++) {
    var option = $genderOptions[i];
    if (option.value === genderValue) 
    {
      option.selected = true;
      break;
    }
  }

  const dateStr = rowElements[4].innerHTML;
  const [year, month, day] = dateStr.split("-").map(Number);

  const date = new Date(year, month - 1, day + 1);
  const dateString = date.toISOString().split("T")[0];

  const input = document.querySelector('input[type="date"]');
  input.value = dateString;
}

$(document).on("click", ".edit-btn", EditStudentOpen);
