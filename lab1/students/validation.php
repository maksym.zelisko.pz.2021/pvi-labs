<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $json = file_get_contents('php://input');
  $data = json_decode($json);
  
  // Validate the name
  if (empty($data->fullName)) {
    $error = array('status' => 'false', 'errorMessage' => 'Field for full name is empty');
    echo json_encode($error);
   
  } elseif (!preg_match('/^[a-zA-Z ]{3,30}$/', $data->fullName)) {
    $error = array('status' => 'false', 'errorMessage' => 'Incorrect name');
    echo json_encode($error);
  } else {
    $error = array('status' => 'true');
    echo json_encode($error);
  }

  if (empty($data->birthDay)) {
    $error = array('status' => 'false', 'errorMessage' => 'Field for birth day is empty');
    echo json_encode($error);
  } elseif (strtotime($data->birthDay) > strtotime(date('Y-m-d'))) {
    $error = array('status' => 'false', 'errorMessage' => 'Date is after today');
    echo json_encode($error);
  } else {
    $error = array('status' => 'true');
    echo json_encode($error);
  }
}
?>